import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider

class FakeSlider(object):
    def __init__(self, slider, func):
        self.func, self.slider = func, slider
    @property
    def val(self):
        return self.func(self.slider.val)

def rk4Algor( t , h , N, y , f ) :
    k1=np.zeros(N) ;
    k2=np.zeros(N) ;
    k3=np.zeros(N) ;
    k4=np.zeros(N) ;
    k1 = h* f( t , y )
    k2 = h* f(t+h/2,y+k1/2)
    k3 = h* f(t+h/2,y+k2/2)
    k4 = h* f(t+h,y+k3 )
    y=y+(k1+2*(k2+k3)+k4)/6
    return y


def PlmFunc(el,m):
    n = 1999

    Plm = np.zeros((n))
    y = np.zeros(2) ; dCos = 0.001

    if el == 0 or el == 2 : y[ 0 ] = 1
    if ( el >2 and ( el )%2 == 0 ) :
        if m == 0 : y[ 0 ] = -1
        elif ( m>0 ) : y[ 0 ] = 1
        elif m<0 and abs(m)%2 == 0 : y[ 0 ] = 1
        elif m<0 and abs(m)%2 == 1 : y[ 0 ] = -1
    if ( el >2 and el %2 == 1 ) :
        if m == 0: y[ 0 ] = 1
        elif m>0: y[ 0 ] = -1
        elif m<0: y[ 0 ] = 1
    y[1] = 1

    def f(Cos ,y) :
        rhs = np.zeros(2)
        rhs [ 0 ] = y[1]
        rhs [ 1 ] = 2* Cos *y[ 1 ] / ( 1 - Cos ** 2 ) -( el * ( el +1)\
        -m**2/(1 - Cos**2 ) ) *y[ 0 ] / ( 1 - Cos ** 2 )
        return rhs

    i = -1
    for Cos in np.arange( -0.999999, 1-dCos , dCos):
        i = i + 1
        y = rk4Algor(Cos, dCos,2,y,f)
        Plm[ i ] = y[ 0 ]
    Plm = Plm/max(abs(Plm))

    return Plm

init_el = 2
init_m = 0
dCos = 0.001
CosTheta = np.arange( -0.999999, 1-dCos , dCos)
fig, ax = plt.subplots()
ax.set_title("Normalized $ \mathbf{ P_l ^ m ( Cos ) } $")
ax.set_xlabel(r"Cos($\theta$)")
ax.set_ylabel(r"$ \mathbf{P_l^m(Cos(\theta)}$")
line, = ax.plot(CosTheta, PlmFunc(init_el,init_m),
                 label=f"$l=${init_el},$m$={init_m}", lw=2)
ax.set_ylim([-1,1])
ax.legend()

plt.subplots_adjust(bottom=0.35)


allowed_el = [i for i in range(1,7)]
allowed_m = [i for i in range(-6,7)]
ax_el = plt.axes([0.15, 0.1, 0.65, 0.03])
el_slider = Slider(
    ax=ax_el,
    label='$l$',
    valmin=1,
    valmax=6,
    valstep=allowed_el,
    valinit=init_el,
)

ax_m = plt.axes([0.15, 0.15, 0.65, 0.03])
m_slider = Slider(
    ax=ax_m,
    label='$m$',
    valmin=-6,
    valmax=6,
    valstep=allowed_m,
    valinit=init_m,
    slidermin=FakeSlider(el_slider, lambda x: -x),
    slidermax=el_slider
)

def update(val):
    line.set_ydata(PlmFunc(el_slider.val, m_slider.val))
    line.set_label(f"$l=${el_slider.val},$m$={m_slider.val}")
    ax.legend()
    fig.canvas.draw_idle()

el_slider.on_changed(update)
m_slider.on_changed(update)

ax.grid()
plt.show()
