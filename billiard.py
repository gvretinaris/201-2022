#!/usr/bin/env python
import matplotlib as mpl
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import sys

plt.rcParams['font.size']=14

# dpi = 100
fig = plt.figure()
# fig = plt.figure(figsize=(8,8))
# fig = plt.figure(figsize=(800/dpi,800/dpi),dpi=dpi)
ax = fig.add_subplot(projection="3d")
#line, = ax.plot([], [], [], lw=2)

Nmax = 101 ; dt = 0.01 ;
Tmax = 200 ; Kx = 10. ;
dx = 0.2 ; dx2 = dx*dx ;
Ky = 15. ; xin = 50;
fc = dt / dx2
yin = 50.
I = np.zeros((Nmax,Nmax)) ; R = np.zeros((Nmax,Nmax))


wframe = None
# animation function.  This is called sequentially
def animate(k):
    global wframe
    if wframe:
        ax.collections.remove(wframe)
        ax.figure.canvas.draw()
    if k == 0:
        for i in range(1,Nmax-1):
            for j in range(1,Nmax-1):
                Gauss = np.exp(-.05*( i-yin)**2-.05*(j-xin)**2)
                R [ i , j ] = Gauss *np.cos(Kx* j + Ky* i )
                I [ i , j ] = Gauss *np.sin(Kx* j + Ky* i )

    R[1:-1,1:-1]= R[1:-1,1:-1] - fc*(I[2:,1:-1] + I[0:-2,1:-1]\
        -4*I[1:-1,1:-1] + I[1:-1,2:] + I[1:-1,0:-2] )
    I[1:-1,1:-1] =I[1:-1,1:-1] + fc*(R[2:,1:-1] + R[0:-2,1:-1]\
        -4*R[1:-1,1:-1] + R[1:-1,2:] + R[1:-1,0:-2])


    x = y = np.arange(0,Nmax)
    X, Y = np.meshgrid(x,y)
    Z = ( I**2 + R**2 )
    wframe = ax.plot_wireframe(X ,Y , Z, rstride=1, cstride=1, color='k', linewidth=0.5)
    ax.set_xlabel( "x" ) ;
    ax.set_ylabel( "y" ) ;
    ax.set_zlabel( "$\Psi$" )
    ax.set_title(f"$\Psi ( x , y , t )$ at {k} Times on Square Billiard Table")
    ax.set_zlim([0,3]) 
   # plt.savefig(f"gif/billiard_{k+1}.png")
    return ax,

# call the animator.  blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(fig, animate,
                              frames=Tmax, interval=1,
                               blit=True,repeat=True,
                               )
plt.show()
plt.close()
